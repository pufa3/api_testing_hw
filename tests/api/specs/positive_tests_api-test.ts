import { expect } from "chai";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { RegisterController } from '../lib/controllers/register.controller';
import { AuthController } from '../lib/controllers/auth.controller';
import { PostController } from "../lib/controllers/post.controller";

const register = new RegisterController();
const auth = new AuthController();
const posts = new PostController();

xdescribe('User Registration, Login, and Post Creation', () => {

    const testUserData: {
        id: number;
        avatar: string;
        email: string;
        userName: string;
        password: string;
    } = {
        id: 0,
            avatar: "Image",
            email: "testpufa234@gmail.com",
            userName: "Pufa Happy",
            password: "qwerty",
    };

    let accessToken: string;
    let validUserId: number;
    let validPostId: number;

    it(`should register a new user`, async () => {
        const registerResponse = await register.register(testUserData);
        checkStatusCode(registerResponse, 201);
        validUserId = registerResponse.body.user.id;
    });

    it(`should login with the registered user`, async () => {
        const loginResponse = await auth.login(testUserData.email, testUserData.password);
        checkStatusCode(loginResponse, 200);
        checkResponseTime(loginResponse, 1000);
        accessToken = loginResponse.body.token.accessToken.token;
    });

    it(`should create a new post`, async () => {
        const postResponse = await posts.createNewPost(validUserId, "test.image", "Hello, Hello!!!", accessToken);
        checkStatusCode(postResponse, 200);
        validPostId = postResponse.body.id;
    });

    
});
