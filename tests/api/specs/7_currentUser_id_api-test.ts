    import { expect } from "chai";
    import { UsersController } from "../lib/controllers/users.controller";
    import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

    const users = new UsersController();
    const schemas = require('./data/schemas_testData.json');
    const chai = require('chai');
    chai.use(require('chai-json-schema'));

    xdescribe("Get User Details by ID", () => {
            let userId: number;
        
            before(`Get all users and store user ID`, async () => {
                const response = await users.getAllUsers();
                checkStatusCode(response, 200);
                checkResponseTime(response, 1000);
                expect(response.body.length, `Response body should have more than 1 item`).to.be.greaterThan(1);
                expect(response.body).to.be.jsonSchema(schemas.schema_allUsers); 
                
                userId = response.body[3].id;
            });
        
            it(`should return details of the user based on the provided ID`, async () => {
                const response = await users.getUserById(userId);
                checkStatusCode(response, 200);
                checkResponseTime(response, 1000);
        
                //
                expect(response.body).to.have.property('id');
                expect(response.body).to.have.property('avatar');
                expect(response.body).to.have.property('email');
                expect(response.body).to.have.property('userName');
        
                console.log("Response Body:", response.body);
                console.log("Status Code:", response.statusCode);
            });
        });