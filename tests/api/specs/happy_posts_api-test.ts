    import { expect } from "chai";
    import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
    import { RegisterController } from '../lib/controllers/register.controller';
    import { AuthController } from '../lib/controllers/auth.controller';
    import { PostController } from "../lib/controllers/post.controller";
    import { UsersController } from "../lib/controllers/users.controller";

    const register = new RegisterController();
    const auth = new AuthController();
    const posts = new PostController();
    const users = new UsersController();

    xdescribe('Post happy path', () => {

        let testUserData: {
            id: number;
            avatar: string;
            email: string;
            userName: string;
            password: string;
        } = {
            id: 0,
            avatar: "Image",
            email: "testpufa234@gmail.com",
            userName: "Pufa Happy",
            password: "qwerty",
        };
        
        let accessToken: string;
        let validUserId: number;
        let validPostId: number;
        let userEmail: string;
        let userPassword: string;

        let previewImage: string = "test";
        let postBody: string = "Hello"; 

        before(`Registration with valid data`, async () => {
            const response = await register.register(testUserData);
            checkStatusCode(response, 201);
            validUserId = response.body.user.id;
        });

        it(`Login and get the token`, async () => {
            userEmail = testUserData.email;
            userPassword = testUserData.password;

            const response = await auth.login(userEmail, userPassword);
            checkStatusCode(response, 200);
            validUserId = response.body.user.id;
            accessToken = response.body.token.accessToken.token;
        });

        it(`Creating new post with valid data`, async () => {
            const postResponse = await posts.createNewPost(validUserId, previewImage, postBody, accessToken);
            checkStatusCode(postResponse, 200);

            validPostId = postResponse.body.id;
            
            expect(postResponse.body.author.id).to.be.equal(validUserId);
            expect(postResponse.body.previewImage).to.be.equal(previewImage);
            expect(postResponse.body.body).to.be.equal(postBody);
        });
        
        it(`Adding like to post`, async () => {
            const likeResponse = await posts.addLikeToPost({ entityId: validPostId, isLike: true, userId: validUserId }, accessToken);
            checkStatusCode(likeResponse, 200);
            
        });

        it(`Adding comment to post`, async () => {
            const commentBody = "Nice post!";
            const commentResponse = await posts.addCommentToPost({ authorId: validUserId, postId: validPostId, body: commentBody }, accessToken);
            checkStatusCode(commentResponse, 200);
            
        });

        it(`Should return all posts`, async () => {
            const getAllPostsResponse = await posts.getAllPosts();
            checkStatusCode(getAllPostsResponse, 200);
            
        });

    

    });
