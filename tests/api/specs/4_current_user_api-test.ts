import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

xdescribe("Get Current User Details", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        const response = await auth.login("tespufa22@example.com", "testpassword");
        checkStatusCode(response, 200);
        accessToken = response.body.token.accessToken.token;
    });

    it('should return details of the current user based on the token', async () => {
        const response = await users.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        checkResponseTime(response, 1000);

        
        expect(response.body).to.have.property('id');
        expect(response.body).to.have.property('avatar');
        expect(response.body).to.have.property('email');
        expect(response.body).to.have.property('userName');


        console.log("Response Body:", response.body);
        console.log("Status Code:", response.statusCode);
        
    });
});
