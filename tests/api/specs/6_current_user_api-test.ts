import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";

const users = new UsersController();
const auth = new AuthController();

xdescribe("Get Current User Details", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("tespufa22@example.com", "testpassword");
        checkStatusCode(response, 200);

        accessToken = response.body.token.accessToken.token;
    });

    it(`should return details of the current user based on the token`, async () => {
        let response = await users.getCurrentUser(accessToken);
        checkStatusCode(response, 200);

        
        expect(response.headers['content-type']).to.include('application/json');
        expect(response.body).to.have.property('id');
        expect(response.body).to.have.property('avatar');
        expect(response.body).to.have.property('email');
        expect(response.body).to.have.property('userName');
        
       
        console.log("User Details:", response.body);
    });
});
