    import { expect } from "chai";
    import { RegisterController } from "../lib/controllers/register.controller";

    const registerController = new RegisterController();


    xdescribe('User Registration', () => {

        it('should register a new user and return 200 status code', async () => {
        const newUser = {
            id: 0,
            avatar:'https://nextjs.org/_next/image?url=%2Fdocs%2Flight%2Fresponsive-image.png&w=3840&q=75&dpl=dpl_Jjmsiey5ByShKdG6EsKhqUHtG69A',
            email: 'tespufa2222@example.com',
            userName: 'testpufa2222',
            password: 'testpassword',
        };
      
        const response = await registerController.register(newUser);
        expect(response.statusCode, `Status Code should be 201`).to.be.equal(201);
     

    

        });
    });