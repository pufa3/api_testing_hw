import { checkResponseTime, checkStatusCode, } from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
const auth = new AuthController();

xdescribe('Invalid Authentication Scenarios', () => {
    let invalidCredentialsDataSet = [
        { email: 'nonexistentuser@.com', password: 'invalidpassword' },
        { email: '@example.com', password: '1' },
        { email: 'validuser@', password: '' },
        { email: 'v@example.com', password: '//incorrectpassword' },
    ];

    invalidCredentialsDataSet.forEach((credentials) => {
        it(`should not authenticate with invalid credentials : '${credentials.email}' + '${credentials.password}'`, async () => {
            let response = await auth.login(credentials.email, credentials.password);

            checkStatusCode(response, 404); 
            checkResponseTime(response, 1000);
        });
    });
});
