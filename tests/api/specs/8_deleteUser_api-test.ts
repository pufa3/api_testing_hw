import { expect } from "chai";
import { AuthController } from "../lib/controllers/auth.controller";
import { UsersController } from "../lib/controllers/users.controller";
import { checkStatusCode, checkResponseTime } from "../../helpers/functionsForChecking.helper";
import { RegisterController } from "../lib/controllers/register.controller";

const auth = new AuthController();
const users = new UsersController();
const registerController = new RegisterController();

describe("Create, Authenticate, and Delete User", () => {
    let accessToken: string;
    let userIdToDelete: string;

    
    before(async () => {
        
        const newUserResponse = await registerController.register({
            id: 0,
            avatar: "test-avatar-url",
            userName: "testuser",
            email: "testuser@example.com",
            password: "testpassword",
        });
        checkStatusCode(newUserResponse, 201);

       
        const loginResponse = await auth.login("testuser@example.com", "testpassword");
        checkStatusCode(loginResponse, 204);
        checkResponseTime(loginResponse, 1000);
        accessToken = loginResponse.body.token.accessToken.token;

        
        userIdToDelete = newUserResponse.body.user.id.toString();
    });

    it(`should delete the authenticated user`, async () => {
       
        const deleteUserResponse = await users.deleteUserById(userIdToDelete, accessToken);
        checkStatusCode(deleteUserResponse, 200);
        checkResponseTime(deleteUserResponse, 1000);
    });

    
    after(async () => {
       
        const deleteUserResponse = await users.deleteUserById(userIdToDelete, accessToken);
        checkStatusCode(deleteUserResponse, 200);
        checkResponseTime(deleteUserResponse, 1000);
    });
});
