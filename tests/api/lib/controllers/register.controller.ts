import { ApiRequest } from "../request";
const baseUrl: string = global.appConfig.baseUrl;

export class RegisterController {
    async register(newUser: { id: number, avatar: string, userName: string, email: string, password: string }) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method("POST")
            .url(`api/Register`)
            .body(newUser)
            .send();
        return response;
    }
}
