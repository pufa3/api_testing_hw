    import { ApiRequest } from "../request";

    const baseUrl: string = global.appConfig.baseUrl;

    export class PostController {
        async getAllPosts() {
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("GET")
                .url(`api/Posts`)
                .send();
            return response;
        }
        async createNewPost(authId, previewImageValue: string, postBody: string, accessToken: string) {
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("POST")
                .url(`api/Posts`)
                .body({
                    authorId: authId,
                    previewImage: previewImageValue,
                    body: postBody,
                })
                .bearerToken(accessToken)
                .send();
            return response;
        }


        async addLikeToPost(reaction: object, accessToken: string) {
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("POST")
                .url(`api/Posts/like`)
                .bearerToken(accessToken)
                .body(reaction)
                .send();
            return response;
        }

        async addCommentToPost(comment: object, accessToken: string) {
            const response = await new ApiRequest()
                .prefixUrl(baseUrl)
                .method("POST")
                .url(`api/Comments`)
                .body(comment)
                .bearerToken(accessToken)
                .send();
            return response;
        }

    }